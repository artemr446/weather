<?php

use Phalcon\Mvc\Controller;

class GetcitiesController extends Controller
{
    /**
     * Welcome and user list
     */
    public function indexAction()
    {
        $this->view->cities = Cities::find();
    }
}