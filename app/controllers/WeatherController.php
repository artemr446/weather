<?php

use Phalcon\Mvc\Controller;
class WeatherController extends IndexController
{
    public function indexAction()
    {

    }

    public function weatherAction()
    {
        $city = new Cities();

        // Store and check for errors
        $city->save(
            $this->request->getPost(),
            [
                "city",
                "lat",
                "lon"
            ]
        );

        $data = $this->request->getPost();
        if(!empty($data['city']) && (empty($data['lat']) && empty($data['lon']))) {
            $api_request_str = 'https://api.openweathermap.org/data/2.5/weather?q='. $data['city'] . '&appid=8db6488e08271639b46a073861663295';
        } else if(empty($data['city']) && (!empty($data['lat']) && !empty($data['lon']))) {
            $api_request_str = 'https://api.openweathermap.org/data/2.5/weather?lat='. $data['lat'] . '&lon=' . $data['lon'] . '&appid=8db6488e08271639b46a073861663295';
        }

        if(!empty($data['city']) && (!empty($data['lat']) && !empty($data['lon']))) {
            $error = 'ERROR ! Don\'t enter both city and coondinates';
            $this->view->setVar('error', $error);
        }

        $weather = file_get_contents($api_request_str);
        $weather = json_decode($weather);
        $this->view->setVar('weather', $weather);


    }
}